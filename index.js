// NOTE: Don't modify this file, use app.js for all your changes
var fs = require('fs');
fs.readFile('./.babelrc', function(err, source){
  var contents = source.toString();
  var config = JSON.parse(contents);
  require('babel-core/register')(config);
  var sourceFile = (process.env.MODE?'./data/'+process.env.MODE+'.txt':process.argv[2]);
  fs.readFile(sourceFile, function(err, source){
    require('./lib/app').main(err, source?source.toString():source);
  });
});
