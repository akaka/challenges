let reIsTrue = /^(true|t|yes|y)&/;
let reIsFalse = /^(false|f|no|n)&/;
function isTrue(source){
  return !!reIsTrue.exec(source);
}
function isFalse(source){
  return !!reIsFalse.exec(source);
}
function isNumeric(n){
  return !isNaN(parseFloat(n)) && isFinite(n);
}

export function main(err, contents){
  // Your code goes here
  if(err){
    return console.error(err);
  }
  console.log(contents);
};
